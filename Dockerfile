FROM node:14

ARG BUILD_ENV=production

WORKDIR /app
COPY . . 

RUN npm install && \
    npm run build && \
    # Permissions will be set to 660 if BUILD_ENV=development
    bash ./scripts/setDevFilePermissions.sh

EXPOSE 8080
EXPOSE 9229

ENTRYPOINT [ "/bin/bash", "./scripts/start.sh" ]